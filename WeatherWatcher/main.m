	//
//  main.m
//  WeatherWatcher
//
//  Created by Max Rovkin on 19.10.14.
//  Copyright (c) 2014 UseIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
