//
// Created by Max Rovkin on 03.11.14.
// Copyright (c) 2014 UseIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Forecast;

@interface ForecastTimeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property(nonatomic, retain) Forecast *forecast;
@end