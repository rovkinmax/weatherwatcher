//
// Created by Max Rovkin on 29.10.14.
// Copyright (c) 2014 UseIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Forecast;

@protocol WeatherDelegate <NSObject>
-(void)didWeatherLoadSucceeded:(Forecast *)loadedContent;
-(void)didWeatherLoadFailed: ( NSError* )error;
@end