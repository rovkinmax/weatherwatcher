//
//  Time.h
//  WeatherWatcher
//
//  Created by Max Rovkin on 19.10.14.
//  Copyright (c) 2014 UseIT. All rights reserved.
//

#ifndef WeatherWatcher_Time_h
#define WeatherWatcher_Time_h


#endif

#import <Foundation/Foundation.h>

@interface Time : NSObject

@property(copy) NSString *from;
@property(copy) NSString *to;
@property(assign) double precipitationValue;
@property(copy) NSString *precipitationType;
@property(assign) double windDirection;
@property(assign) double windSpeed;
@property(assign) int temperature;
@property(assign) double pressure;
@property(assign) int humidity;
@property(copy) NSString *humanName;
@property(assign) int iconId;
@end
