//
// Created by Max Rovkin on 03.11.14.
// Copyright (c) 2014 UseIT. All rights reserved.
//

#import "ForecastTimeViewController.h"
#import "Forecast.h"
#import "Time.h"


@implementation ForecastTimeViewController {

}

@synthesize forecast = _forecast;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.forecast)
        return [self.forecast.times count];
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:@"tableItem"];
    Time *time = [self.forecast.times objectAtIndex:(NSUInteger) indexPath.row];

    UIImageView *icon = (UIImageView *) [tableViewCell viewWithTag:100];
    icon.image = [self imageByWeatherSymbolId:time.iconId];

    UILabel *name = (UILabel *) [tableViewCell viewWithTag:101];
    name.text = time.humanName;

    UILabel *fromToLabel = (UILabel *) [tableViewCell viewWithTag:102];
    fromToLabel.text = [NSString stringWithFormat:@"с %@ по %@", time.from, time.to];

    return tableViewCell;
}

- (UIImage *)imageByWeatherSymbolId:(int)symbolId {

    switch(symbolId){
        case 200: return [UIImage imageNamed:@"11d.png"];
        case 201: return [UIImage imageNamed:@"11d.png"];
        case 202: return [UIImage imageNamed:@"11d.png"];
        case 210: return [UIImage imageNamed:@"11d.png"];
        case 211: return [UIImage imageNamed:@"11d.png"];
        case 212: return [UIImage imageNamed:@"11d.png"];
        case 221: return [UIImage imageNamed:@"11d.png"];
        case 230: return [UIImage imageNamed:@"11d.png"];
        case 231: return [UIImage imageNamed:@"11d.png"];
        case 232: return [UIImage imageNamed:@"11d.png"];

        case 300: 	return [UIImage imageNamed:@"09d.png"];
        case 301: 	return [UIImage imageNamed:@"09d.png"];
        case 302: 	return [UIImage imageNamed:@"09d.png"];
        case 310: 	return [UIImage imageNamed:@"09d.png"];
        case 311: 	return [UIImage imageNamed:@"09d.png"];
        case 312: 	return [UIImage imageNamed:@"09d.png"];
        case 313: 	return [UIImage imageNamed:@"09d.png"];
        case 314: 	return [UIImage imageNamed:@"09d.png"];
        case 321: 	return [UIImage imageNamed:@"09d.png"];

        case 500:	return [UIImage imageNamed:@"10d.png"];
        case 501:	return [UIImage imageNamed:@"10d.png"];
        case 502:	return [UIImage imageNamed:@"10d.png"];
        case 503:	return [UIImage imageNamed:@"10d.png"];
        case 504:	return [UIImage imageNamed:@"10d.png"];
        case 511:	return [UIImage imageNamed:@"13d.png"];
        case 520:	return [UIImage imageNamed:@"09d.png"];
        case 521:	return [UIImage imageNamed:@"09d.png"];
        case 522:	return [UIImage imageNamed:@"09d.png"];
        case 531:	return [UIImage imageNamed:@"09d.png"];

        case 600: return [UIImage imageNamed:@"13d.png"];
        case 601: return [UIImage imageNamed:@"13d.png"];
        case 602: return [UIImage imageNamed:@"13d.png"];
        case 611: return [UIImage imageNamed:@"13d.png"];
        case 612: return [UIImage imageNamed:@"13d.png"];
        case 615: return [UIImage imageNamed:@"13d.png"];
        case 616: return [UIImage imageNamed:@"13d.png"];
        case 620: return [UIImage imageNamed:@"13d.png"];
        case 621: return [UIImage imageNamed:@"13d.png"];
        case 622: return [UIImage imageNamed:@"13d.png"];

        case 701: return [UIImage imageNamed:@"50d.png"];
        case 711: return [UIImage imageNamed:@"50d.png"];
        case 721: return [UIImage imageNamed:@"50d.png"];
        case 731: return [UIImage imageNamed:@"50d.png"];
        case 741: return [UIImage imageNamed:@"50d.png"];
        case 751: return [UIImage imageNamed:@"50d.png"];
        case 761: return [UIImage imageNamed:@"50d.png"];
        case 762: return [UIImage imageNamed:@"50d.png"];
        case 771: return [UIImage imageNamed:@"50d.png"];
        case 781: return [UIImage imageNamed:@"50d.png"];

        case 800: return [UIImage imageNamed:@"01d.png"];
        case 801: return [UIImage imageNamed:@"02d.png"];
        case 802: return [UIImage imageNamed:@"03d.png"];
        case 803: return [UIImage imageNamed:@"04d.png"];
        case 804: return [UIImage imageNamed:@"04d.png"];


        default:return [UIImage imageNamed:@"01d.png"];
    }
}


- (void)dealloc {
    [_forecast release];
    [super dealloc];
}

@end