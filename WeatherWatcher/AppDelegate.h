//
//  AppDelegate.h
//  WeatherWatcher
//
//  Created by Max Rovkin on 19.10.14.
//  Copyright (c) 2014 UseIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherDelegate.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,WeatherDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

