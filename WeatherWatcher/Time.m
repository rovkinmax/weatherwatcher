//
// Created by Max Rovkin on 19.10.14.
// Copyright (c) 2014 UseIT. All rights reserved.
//

#import "Time.h"


@implementation Time

@synthesize from = _from;
@synthesize to = _to;
@synthesize precipitationValue = _precipitationValue;
@synthesize precipitationType = _precipitationType;
@synthesize windDirection = _windDirection;
@synthesize windSpeed = _windSpeed;
@synthesize temperature = _temperature;
@synthesize pressure = _pressure;
@synthesize humidity = _humidity;
@synthesize humanName = _humanName;
@synthesize iconId = _iconId;

- (instancetype)init {
    self = [super init];
    if (self) {

    }

    return self;
}

- (void)dealloc {
    [_from release];
    [_to release];
    [_precipitationType release];
    [_humanName release];
    [super dealloc];
}
@end