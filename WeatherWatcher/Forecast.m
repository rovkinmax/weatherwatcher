//
// Created by Max Rovkin on 19.10.14.
// Copyright (c) 2014 UseIT. All rights reserved.
//

#import "Forecast.h"
#import "Time.h"


@implementation Forecast
@synthesize times = _times;

- (instancetype)init {
    self = [super init];
    if (self) {
        self.times = [NSMutableArray array];
    }

    return self;
}

-(void)dealloc {

    [_times release];
    [super dealloc];
}

@end