//
// Created by Max Rovkin on 19.10.14.
// Copyright (c) 2014 UseIT. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Forecast : NSObject
@property(retain) NSMutableArray *times;
@end