//
// Created by Max Rovkin on 19.10.14.
// Copyright (c) 2014 UseIT. All rights reserved.
//

#import "Weather.h"
#import "Forecast.h"
#import "WeatherDelegate.h"
#import "Time.h"


@implementation Weather {
@private
    NSString *currentElement;
@private
    Time *tempTime;
}


@synthesize country = _country;
@synthesize city = _city;
@synthesize latitude = _latitude;
@synthesize longitude = _longitude;
@synthesize forecast = _forecast;
@synthesize weatherDelegate = _weatherDelegate;
@synthesize sunRise = _sunRise;
@synthesize sunSet = _sunSet;

- (instancetype)init {
    self = [super init];
    if (self) {
        self.forecast = [[Forecast alloc] init];
    }

    return self;
}

- (void)loadFromFile:(NSString *)fileName target:(id <WeatherDelegate>)target {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"xml"];
    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    NSXMLParser *rssParser = [[NSXMLParser alloc] initWithData:myData];
    self.weatherDelegate = target;
    rssParser.delegate = self;
    [rssParser parse];
    [rssParser release];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([currentElement isEqualToString:@"name"])
        self.city = string;

    if ([currentElement isEqualToString:@"country"])
        self.country = string;

}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    currentElement = elementName;
    if ([elementName isEqualToString:@"location"])
        [self parseLocation:attributeDict];

    if ([elementName isEqualToString:@"sun"]) {
        self.sunRise = attributeDict[@"rise"];
        self.sunSet = attributeDict[@"set"];
    }

    [self parseTime:elementName attributeDict:attributeDict];

}

- (void)parseLocation:(NSDictionary *)attributeDict {
    if ([attributeDict count] > 0) {
        self.latitude = [attributeDict[@"latitude"] doubleValue];
        self.longitude = [attributeDict[@"longitude"] doubleValue];
    }
}

- (void)parseTime:(NSString *)elementName attributeDict:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"time"]) {
        tempTime = [[[Time alloc] init] autorelease];
        tempTime.from = attributeDict[@"from"];
        tempTime.to = attributeDict[@"to"];
    }

    if ([elementName isEqualToString:@"windDirection"])
        tempTime.windDirection = [attributeDict[@"deg"] doubleValue];

    if ([elementName isEqualToString:@"windSpeed"])
        tempTime.windSpeed = [attributeDict[@"mps"] doubleValue];

    if ([elementName isEqualToString:@"temperature"])
        tempTime.temperature = [attributeDict[@"value"] integerValue];

    if ([elementName isEqualToString:@"pressure"])
        tempTime.pressure = [attributeDict[@"value"] doubleValue];

    if ([elementName isEqualToString:@"humidity"])
        tempTime.humidity = [attributeDict[@"value"] integerValue];

    if ([elementName isEqualToString:@"precipitation"]) if ([attributeDict count] > 0) {
        tempTime.precipitationValue = [attributeDict[@"value"] doubleValue];
        tempTime.precipitationType = attributeDict[@"type"];
    }

    if ([elementName isEqualToString:@"symbol"]) {
        tempTime.iconId = [attributeDict[@"number"] integerValue];
        tempTime.humanName = attributeDict[@"name"];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    currentElement = nil;
    if ([elementName isEqualToString:@"time"]) {
        [self.forecast.times addObject:tempTime];
        tempTime = nil;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    if (self.weatherDelegate)
        [self.weatherDelegate didWeatherLoadSucceeded:self.forecast];
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    if (self.weatherDelegate)
        [self.weatherDelegate didWeatherLoadFailed:parseError];
}


- (void)dealloc {
    [self.forecast release];
    [_country release];
    [_city release];
    [_weatherDelegate release];
    [_sunSet release];
    [_sunRise release];
    [super dealloc];
}


@end
