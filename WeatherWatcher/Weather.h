//
// Created by Max Rovkin on 19.10.14.
// Copyright (c) 2014 UseIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Forecast;
@protocol WeatherDelegate;


@interface Weather : NSObject <NSXMLParserDelegate>

@property(copy) NSString *country;
@property(copy) NSString *city;
@property(assign) double latitude;
@property(assign) double longitude;
@property(copy) NSString *sunRise;
@property(copy) NSString *sunSet;
@property(strong) Forecast *forecast;
@property(retain) id <WeatherDelegate> weatherDelegate;

-(void) loadFromFile:(NSString *)fileName target:(id<WeatherDelegate>)target;
@end